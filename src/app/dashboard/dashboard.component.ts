import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { Group } from '../group';

import { HeroService } from '../hero.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
  groups: Group[] = [];
  constructor(private heroService: HeroService) {}

  ngOnInit() {
    this.getHeroes();
    this.getGroups();
  }

  getHeroes(): void {
    this.heroService
      .getHeroes()
      .subscribe((heroes) => (this.heroes = heroes.slice(1, 5)));
  }

  getGroups(): void {
    this.heroService.getGroups().subscribe((groups) => (this.groups = groups));
  }
}
