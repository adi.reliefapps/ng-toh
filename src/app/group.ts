import { StringifyOptions } from 'querystring';

export interface Group {
  name: string;
  id: number;
}
