import { Component, OnInit, Input } from '@angular/core';
import { Group } from '../group';
import { Hero } from '../hero';

import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { HeroService } from '../hero.service';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.css'],
})
export class GroupDetailComponent implements OnInit {
  @Input() heroes: Hero[];

  getHeroesByGroup(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHeroes().subscribe((heroes: any[]) => {
      this.heroes = heroes.filter((x) => x.group === id);
      console.log('this.heroes', this.heroes);
    });
  }

  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getHeroesByGroup();
  }
}
